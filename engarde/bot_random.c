#include <stdio.h>

#define NHAND 5
#define NFIELD 23

int cards[NHAND];
int enemy_pos;
int bot_pos;
int player;

FILE* flog;

void initialize(char *buf)
{
    int ch;  
	char fname[1024];
    sscanf(buf, "%c %d %d %d %d %d %d", &ch, &player, &cards[0], &cards[1], &cards[2], &cards[3], &cards[4]);
    if (player == 1)
	{
        bot_pos = 0;
        enemy_pos = NFIELD-1;
    } 
	else if (player == 2) 
	{
        bot_pos = NFIELD-1;
        enemy_pos = 0;
    } 
	else 
        fprintf(stderr, "invalid player number\n");
	//char fname[1024]; 
    sprintf(fname, "log%d.txt", player);
    flog = fopen(fname, "w");
}

int is_correct_move(int k){
    if (k == 0)
        return 0;

    if (bot_pos + k < 0)
        return 0;

    if (bot_pos + k >= NFIELD)
        return 0;

    if (bot_pos < enemy_pos)
	{
        if (bot_pos + k > enemy_pos)
            return 0;
	}
	else 
	{
        if (bot_pos + k < enemy_pos)
            return 0;
	}
    return 1;
}

void bot_move()
{
    //choose random card
    int k,j=0; int i;
    do
	{
        k = cards[ rand() % NHAND ];
        if (rand() % 2 == 0)
            k = -k;
        j++;
        if (j >= 100)
		{
            k = 0;
            break;
		}
    }while(!is_correct_move(k));
    //mark choosed card as zero
    
    for(i=0; i<NHAND; i++)
        if (cards[i] == k || cards[i] == -k)
		{
            cards[i] = 0;
            break;
        }

    bot_pos += k;

    //print card
    printf("m %d\n", k); fprintf(flog, "bot_move %d\n", k);
    fflush(stdout);
}

void bot_recieve_card(char *line)
{
    char ch;
    int card;
	int i;
    sscanf(line, "%c %d", &ch, &card); fprintf(flog, "receive %d\n", card);

    //put card to hand
    
    for(i=0; i<NHAND; i++)
        if (cards[i] == 0)
		{
            cards[i] = card;
            break;
        }
}

void enemy_move(char *line)
{
    char ch;
    int card;
    sscanf(line, "%c %d", &ch, &card);
    enemy_pos += card; fprintf(flog, "enemy_move %d  line %s\n", card, line);
}

void print_game()
{
    int i;
    for(i=0; i<NFIELD; i++)
	{
        if (bot_pos != i && enemy_pos != i)
            fprintf(flog, "- ");
        else
            fprintf(flog, "* ");
    }
    fprintf(flog, "\n");
    fprintf(flog, "bot_pos=%d enemy_pos=%d\n", bot_pos, enemy_pos);
    //fprintf(flog, "bot_pos=%d enemy_pos=%d\n", bot_pos, enemy_pos);
}

void main()
{
    int loop = 1;
    char line[1024];
    while(loop)
	{
        fgets(line, 1023, stdin);
        switch(line[0])
		{
        case 'i':
            initialize(line);
            if (player == 1)
                bot_move();
            break;
        case 'm':
            enemy_move(line);
            bot_move();
            break;
        case 'd':
            bot_recieve_card(line);
            break;
        case 'f':
            loop = 0;
            break;
        default:
            fprintf(stderr, "invalid command\n");
            break;
        }
        print_game();
    }
    fclose(flog);
}
