#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

#define SIZE 10

void print_array(int a[]){
	for(int i=0; i<SIZE; i++){
		printf("%d ", a[i]);
	}
	printf("\n");
}

void sort_select(int a[]){
	for(int i=0; i<SIZE; i++){
		int min = i;
		for(int j=i+1; j<SIZE; j++){
			if (a[j] < a[min]){
				min = j;
			}
		}
		int k = a[min];
		a[min] = a[i];
		a[i] = k;		
		print_array(a);
	}
}

int main(){
	srand(time(NULL));
	int a[SIZE];
	for(int i=0; i<SIZE; i++){
		a[i] = rand() % 10;
	}
    print_array(a);
	sort_select(a);	
	return 0;
}
