#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

#define SIZE 10

void print_array(int a[]){
	for(int i=0; i<SIZE; i++){
		printf("%d ", a[i]);
	}
	printf("\n");
}

void sort_select(int a[]){
	for(int i=0; i<SIZE; i++){
		int min = i;
		for(int j=i+1; j<SIZE; j++){
			if (a[j] < a[min]){
				min = j;
			}
		}
		int k = a[min];
		a[min] = a[i];
		a[i] = k;		
	}
}

int search_binary(int a[], int key){
	int pos = -1;
	int low = 0;
	int high = SIZE-1;

	int i = 0;

	while(high >= low) {
		int mid = (low + high)/2;
		printf("low=%d high=%d mid=%d\n", low, high, mid);		
		if (a[mid] < key) {
			low = mid+1;
		} else if (a[mid] > key) {
			high = mid-1;
		} else {
			return mid;
		}
	}
	return pos;
}

int main(){
	srand(time(NULL));
	int a[SIZE];
	for(int i=0; i<SIZE; i++){
		a[i] = rand() % 10;
	}
	sort_select(a);	
	print_array(a);

    int key = rand() % 11;
	printf("key %d found at %d\n", key, search_binary(a, key));

	return 0;
}
