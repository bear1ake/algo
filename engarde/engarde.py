from random import randint 
import subprocess

program1="/home/vitek/Documents/Courses/Algorithm/engarde/arch" 
program2="/home/vitek/Documents/Courses/Algorithm/engarde/build-bot-Desktop-Debug/bot"
program2=program1

p1 = subprocess.Popen([program1],
                     stdin=subprocess.PIPE,
                     stdout=subprocess.PIPE)
p2 = subprocess.Popen([program2],
                     stdin=subprocess.PIPE,
                     stdout=subprocess.PIPE)

players = [p1, p2]
cards = range(25)
NCARDS = 25
NFIELD = 23
idx = 0

def init_cards(cards):
    for i in range(25):
        cards[i] = ((i+1) % 5) + 1


def shuffle_cards(cards):
    n = randint(100, 1000)
    for p in range(n):
        i = randint(0, 1000) % NCARDS
        j = randint(0, 1000) % NCARDS    
        k = cards[i]
        cards[i] = cards[j]
        cards[j] = k

init_cards(cards)
shuffle_cards(cards)
k = 0
p1.stdin.write("i 1 %d %d %d %d %d\n" % (cards[0], cards[1], cards[2], cards[3], cards[4]))
p2.stdin.write("i 2 %d %d %d %d %d\n" % (cards[5], cards[6], cards[7], cards[8], cards[9]))
idx = 10

class Player:
    def __init__(self, num, process, pos, cards):
        self.process = process
        self.pos = pos
        self.cards = cards
        self.num = num
    def read_move(self):
        line = self.process.stdout.readline()
        move = int(line[1:])
        return move
    def has_card(self, move):
        for i in range(5):
            if self.cards[i] == move or self.cards[i] == -move:
                return True
        return False
    def remove_card(self, card):
        for i in range(5):
            if self.cards[i] == card or self.cards[i] == -card:
                self.cards[i] = 0
                break
    def is_correct_move(self, move, enemy):
        if (self.pos + move < 0):
            return False       
        if (self.pos + move >= NFIELD):
            return False;
        if (self.pos < enemy.pos):
            if (self.pos + move > enemy.pos):
                return False            
        else:
            if (self.pos + move < enemy.pos):
                return False
        return True
    def receive_card(self, card):
        for i in range(5):
            if self.cards[i] == 0:
                self.cards[i] = card
                break
        self.process.stdin.write("d %d\n" % (card))
        print "Player%d receives %d" % (self.num, card)
    def turn(self, enemy):
        card = self.read_move()
        if card == 0:
            print "Player%d surrended" % (self.num)
            return False                
        if not self.has_card(card):
            print "Player%d has not card %d" % (self.num, card)
            return False
        if not self.is_correct_move(card, enemy):
            print "Player%d move invalid %d" % (self.num, card)
            return False
        print "Player%d moves %d" % (self.num, card)        
        if self.pos + card == enemy.pos:
            print "Player%d wins" % (self.num)
            return False
        self.remove_card(card)
        self.pos = self.pos + card        
        enemy.process.stdin.write("m %d\n" % (card))
        return True

def print_game(players):
    for i in range(NFIELD):
        if i != players[0].pos and i != players[1].pos:
            print "-",
        else:
            print "*",
    print
    print "p1 cards: ", players[0].cards 
    print "p2 cards: ", players[1].cards 

ppp1 = Player(1, p1, 0, [cards[0], cards[1], cards[2], cards[3], cards[4]] )
ppp2 = Player(2, p2, NFIELD-1, [cards[5], cards[6], cards[7], cards[8], cards[9]] )
idx = 10
players = [ppp1, ppp2]
result = True
print_game(players)
while result:
    player = players[k]
    enemy = players[ (k+1) % 2 ]

    result = player.turn(enemy)

    if idx >= NCARDS:
        print "Cards over"
        result = False    
        break

    nextcard = cards[idx]    
    player.receive_card(nextcard)
    idx = idx + 1    
    k = (k+1) % 2
    print_game(players)
print_game(players)
p1.stdin.write("f\n")
p2.stdin.write("f\n")
