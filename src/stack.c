#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

#define SIZE 5

int a[SIZE];
int index;

void push(int element){
    if (index < SIZE) {
        a[index++] = element;
    } else {
        printf("Stack is full\n");
    }
}

int pop(){
    int element = -1;
    if (index > 0) {
        element = a[--index];
    } else {
        printf("Stack is empty\n");
    }
    return element;    
}

int main(){
	srand(time(NULL));
	for(int i=0; i<SIZE+1; i++){
	    int element = rand() % 10;
	    push(element);
	    printf("push <- %d\n", element);
	}
    for(int i=0; i<SIZE+1; i++){
	    int element = pop();
	    printf("pop -> %d\n", element);
	}
	for(int i=0; i<SIZE; i++){
	    int element = rand() % 10;
	    push(element);
	    printf("push <- %d\n", element);
	}
    for(int i=0; i<SIZE; i++){
	    int element = pop();
	    printf("pop -> %d\n", element);
	}	
	return 0;
}
