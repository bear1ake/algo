#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

#define SIZE 5

int a[SIZE + 1];
int head = 0;
int tail = 0;

void enqueue(int element){
    int k = (tail + 1) % (SIZE + 1);
    if (k == head) {
        printf("Queue is full\n");
    } else {
        a[tail] = element;
        tail = k;    
    }
}

int dequeue(){
    int element = -1;
    if (head == tail) {
        printf("Queue is empty\n");
    } else {
        element = a[head];
        head = (head + 1) % (SIZE + 1);
    }
    return element;    
}

int main(){
	srand(time(NULL));
	for(int i=0; i<SIZE; i++){
	    int element = rand() % 10;
	    enqueue(element);
	    printf("enqueue <- %d\n", element);
	}
    for(int i=0; i<SIZE/2; i++){
	    int element = dequeue();
	    printf("dequeue -> %d\n", element);
	}
	for(int i=0; i<SIZE/2+1; i++){
	    int element = rand() % 10;
	    enqueue(element);
	    printf("enqueue <- %d\n", element);
	}
    for(int i=0; i<SIZE+1; i++){
	    int element = dequeue();
	    printf("dequeue -> %d\n", element);
	}	
	return 0;
}
