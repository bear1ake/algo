#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>

#define SIZE 10

void print_array(int a[]){
	for(int i=0; i<SIZE; i++){
		printf("%d ", a[i]);
	}
	printf("\n");
}

void sort_insert(int a[]){
	for(int i=1; i<SIZE; i++){
		int j = i;
		while(a[j] < a[j-1] && j > 0 ){
			int k = a[j];
			a[j] = a[j-1];
			a[j-1] = k;

			j--;
		}
		print_array(a);
	}
}

int main(){
	srand(time(NULL));
	int a[SIZE];
	for(int i=0; i<SIZE; i++){
		a[i] = rand() % 10;
	}
	print_array(a);
	sort_insert(a);	
	return 0;
}
